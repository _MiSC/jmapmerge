# JMapMerge

Merge multiple JourneyMap map files.

## Sample Usage
```
# Merge all the maps in ~/Dropbox/Public/MineCraftTopo/
# writing the results to ~/Dropbox/Public/MineCraftTopo/Master
java com.misc.JMapMerger ~/Dropbox/Public/MineCraftTopo/ ~/Dropbox/Public/MineCraftTopo/Master
```
