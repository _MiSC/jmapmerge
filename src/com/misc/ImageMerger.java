package com.misc;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.BinaryOperator;

import javax.imageio.ImageIO;

/**
 *
 */
public class ImageMerger {

    public static void merge(final String outFile, final List<String> inFiles) {
        inFiles.stream()
                .map(ImageMerger::readImage)
                .reduce(merge())
                .ifPresent(imageBits -> writeFile(outFile, imageBits));
    }

    private static BufferedImage readImage(final String path) {
        try {
            return ImageIO.read(new File(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // merge two images (bitwise directly)
    private static BinaryOperator<BufferedImage> merge() {
        return (aImage, bImage) -> {
            final Raster aRaster = aImage.getRaster();
            final Raster bRaster = bImage.getRaster();

            final int w = aRaster.getWidth();
            final int h = aRaster.getHeight();

            int[] a = null;
            int[] b = null;
            int[] out = new int[w * h];

            final WritableRaster outRaster = aRaster.createCompatibleWritableRaster();
            for (int x = 0; x < w; x++) {
                for (int y = 0; y < h; y++) {
                    a = aRaster.getPixel(x, y, a);
                    b = bRaster.getPixel(x, y, b);

                    for (int i = 0; i < a.length; i++) {
                        out[i] = a[i] | b[i];
                    }
                    outRaster.setPixel(x, y, out);
                }
            }

//            a = aRaster.getPixels(0, 0, w, h, a);
//            b = bRaster.getPixels(0, 0, w, h, b);

//            for (int i = 0; i < w * h; i++) {
//                if (a[i] != b[i]) {
//                    System.out.println(i + ", (" + a[i] + ", " + b[i] + ")");
//                }
//                out[i] = a[i] | b[i];
//            }
//            outRaster.setPixels(0, 0, w, h, out);

            aImage.setData(outRaster);
            return aImage;
        };
    }

    // write a bitset out
    private static void writeFile(final String path, final BufferedImage image) {
        try {
            final Path output = new File(path).toPath();
            final Path parentDir = output.getParent();

            if (!Files.exists(parentDir)) {
                Files.createDirectories(parentDir);
            }

            ImageIO.write(image, "png", new File(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
