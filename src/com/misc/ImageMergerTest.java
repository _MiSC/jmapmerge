package com.misc;


import com.google.common.collect.ImmutableList;
import org.testng.annotations.Test;

/**
 *
 */
public class ImageMergerTest {

    @Test
    public void testMergeTwoFiles() {
        ImageMerger.merge(TEST.out, ImmutableList.of(TEST.aIn, TEST.bIn));
    }

    @Test
    public void testMergeThreeFiles() {
        ImageMerger.merge(TEST.out, ImmutableList.of(TEST.aIn, TEST.bIn, TEST.cIn));
    }

    private static class TEST {
        final static String aIn = "test-data/Mike G/DIM0/0/-1,-1.png";
        final static String bIn = "test-data/MiSC/DIM0/0/-1,-1.png";
        final static String cIn = "test-data/Xen/DIM0/0/-1,-1.png";
        final static String out = "test-data/out/Master/DIM0/0/-1,-1.png";
    }
}