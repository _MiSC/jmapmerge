package com.misc;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public class JMapMerger {


    public static void main(final String[] args) throws IOException {

        final String sourceDir = args[0];
        System.out.println("sourceDir: " + sourceDir);
        final Path sourcePath = new File(sourceDir).toPath();

        final String outputDir = args[1];
        System.out.println("outputDir: " + outputDir);
        final Path outputPath = new File(outputDir).toPath();

        // Get the user directories
        final Set<Path> userDirs = getChildPaths(sourcePath)
                .filter(Files::isDirectory)
                .filter(path -> !path.equals(sourcePath))
                .collect(Collectors.toSet());

        userDirs.forEach(System.out::println);

        // get the map files for each user
        Map<Path, List<RelativeToUserPath>> relativeToUserPathMappings =
                userDirs.stream()
                        .flatMap(userDir ->
                                walk(userDir)
                                        .map(userDir::relativize)
                                        .map(relativePath -> new RelativeToUserPath(userDir.resolve(relativePath),
                                                relativePath))
                        )
                        .filter(path -> path.toString().endsWith(".png"))
                        // group the map files across users into a multimap<relativeMapPath>
                        .collect(Collectors.groupingBy(RelativeToUserPath::getRelativeMapPath));

        relativeToUserPathMappings.forEach((key, value) -> {

            final String outputMap = outputPath.resolve(key.toString()).toString();
            final List<String> inputMaps = value.stream()
                    .map(RelativeToUserPath::getUserMapPath)
                    .map(Path::toString)
                    .collect(Collectors.toList());


            System.out.println(String.format("k: %s, v: %s", key.toString(), String.join(",", inputMaps)));

            ImageMerger.merge(outputMap, inputMaps);
        });


    }

    private static Stream<Path> getChildPaths(Path path) {
        try {
            return Files.walk(path, 1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Stream<Path> walk(final Path path) {
        try {
            return Files.walk(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static class RelativeToUserPath {

        final Path userMapPath;

        final Path relativeMapPath;

        public RelativeToUserPath(Path userMapPath, Path relativeMapPath) {
            this.userMapPath = userMapPath;
            this.relativeMapPath = relativeMapPath;
        }

        public Path getUserMapPath() {
            return userMapPath;
        }

        public Path getRelativeMapPath() {
            return relativeMapPath;
        }

        @Override
        public String toString() {
            return String.format("userMapPath: %s, relativeMapPath: %s", userMapPath, relativeMapPath);
        }
    }
}
